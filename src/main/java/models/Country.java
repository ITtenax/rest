package models;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.IOException;
import java.util.List;

/**
 * Created by Valentyn on 12/13/2015.
 */
@Entity
@Table(name = "country")
public class Country {
    private int id;
    private String capital;
    private int populationCount;
    private String currency;

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPopulationCount() {
        return populationCount;
    }

    public void setPopulationCount(int populationCount) {
        this.populationCount = populationCount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public Country(int id, String capital, int populationCount, String currency) {
        this.id = id;
        this.capital = capital;
        this.populationCount = populationCount;
        this.currency = currency;
    }

    public Country() {
    }

    public static List<Country> getCountries(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<Country> countries = mapper.readValue(json, new TypeReference<List<Country>>() {
            });
            return countries;
        } catch (IOException e) {
            throw new RuntimeJsonMappingException("invalid json");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Country country = (Country) o;

        if (id != country.id) return false;
        if (populationCount != country.populationCount) return false;
        if (!capital.equals(country.capital)) return false;
        return currency.equals(country.currency);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + capital.hashCode();
        result = 31 * result + populationCount;
        result = 31 * result + currency.hashCode();
        return result;
    }
}
