package utils;

import ru.yandex.qatools.properties.PropertyLoader;
import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;

/**
 * Created by Valentyn on 12/14/2015.
 */
@Resource.Classpath("testing.properties")
public class Properties {
    private static final Properties instance = new Properties();

    private Properties() {
        PropertyLoader.populate(this);
    }

    @Property("main.url")
    private String mainResrURL;

    public String getMainResrURL() {
        return mainResrURL;
    }

    public static Properties getInstance() {
        return instance;
    }
}
