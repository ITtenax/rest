package services;

import models.Country;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.metamodel.ListAttribute;
import java.util.List;

/**
 * Created by Valentyn on 12/13/2015.
 */
@Stateless
public class CountryService {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Country> getAll() {
        TypedQuery<Country> query = entityManager.createQuery("select c from Country c", Country.class);
        return query.getResultList();
    }

    public void removeCountry(int id) {
        entityManager.createQuery("delete from Country c where c.id=" + id).executeUpdate();
    }

    public void createCountry(int id, String capital, int populationCount, String currency) {
        String query = String.format("insert into COUNTRY (id, CAPITAL, POPULATIONCOUNT, CURRENCY) VALUES (%s,'%s',%s,'%s')", id, capital, populationCount, currency);
        entityManager.createNativeQuery(query).executeUpdate();
    }

    public void updateCountry(int id, String capital, int populationCount, String currency) {
        entityManager.merge(new Country(id, capital, populationCount, currency));
    }
}
