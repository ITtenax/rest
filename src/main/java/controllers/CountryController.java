package controllers;

import models.Country;
import services.CountryService;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.ws.rs.*;
import java.util.function.Predicate;

/**
 * Created by Valentyn on 12/13/2015.
 */
@Path("/countries")
public class CountryController {
    @Inject
    private CountryService countryService;

    @GET
    @Path("/all")
    @Produces("application/json")
    public JsonArray getAll() {
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        countryService.getAll().stream().forEach((country) ->
                jsonArrayBuilder.add(Json.createObjectBuilder()
                        .add("id", country.getId())
                        .add("capital", country.getCapital())
                        .add("populationCount", country.getPopulationCount())
                        .add("currency", country.getCurrency()))
        );
        return jsonArrayBuilder.build();
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public JsonArray getCountry(@PathParam("id") final int id) {
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        Country entity = countryService.getAll().stream().filter(country -> country.getId() == id).findFirst().get();
        jsonArrayBuilder.add(Json.createObjectBuilder()
                .add("id", entity.getId())
                .add("capital", entity.getCapital())
                .add("populationCount", entity.getPopulationCount())
                .add("currency", entity.getCurrency()));
        return jsonArrayBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    public JsonArray removeCountry(@PathParam("id") final int id) {
        countryService.removeCountry(id);
        return getAll();
    }

    @POST
    @Path("/")
    @Produces("application/json")
    public JsonArray createCountry(@FormParam("id") int id,
                                   @FormParam("capital") String capital,
                                   @FormParam("populationCount") int populationCount,
                                   @FormParam("currency") String currency) {
        countryService.createCountry(id, capital, populationCount, currency);
        return getAll();
    }


    @PUT
    @Path("/{id}")
    @Produces("application/json")
    public JsonArray updateCountry(@PathParam("id") int id,
                                   @FormParam("capital") String capital,
                                   @FormParam("populationCount") int populationCount,
                                   @FormParam("currency") String currency) {
        countryService.updateCountry(id, capital, populationCount, currency);
        return getAll();
    }
}
