package tests.services;

import models.Country;
import org.hamcrest.Matchers;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.Test;
import tests.BaseTest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.http.HttpMethod.*;
import static utils.Properties.getInstance;

/**
 * Created by Valentyn on 12/13/2015.
 */
public class RestTest extends BaseTest {
    @Test
    public void verifyGetRequestAllCountries() {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity payload = new HttpEntity(headers);

        ResponseEntity<String> response = restTemplate.exchange(getInstance().getMainResrURL() + "all", GET, payload, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void verifyGetRequestCountry() {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity payload = new HttpEntity(headers);

        ResponseEntity<String> response = restTemplate.exchange(getInstance().getMainResrURL() + "1", GET, payload, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test(dataProvider = "test3")
    public void verifyPutRequest(Country country) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("capital", country.getCapital());
        body.add("populationCount", String.valueOf(country.getPopulationCount()));
        body.add("currency", country.getCurrency());


        HttpEntity<MultiValueMap<String, String>> payload = new HttpEntity<>(body, headers);

        ResponseEntity<String> response = restTemplate.exchange(getInstance().getMainResrURL() + country.getId(),
                PUT, payload, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(Country.getCountries(response.getBody()), hasItem(country));
    }

    @Test(dataProvider = "test4")
    public void verifyPostRequest(Country country) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("id", String.valueOf(country.getId()));
        body.add("capital", country.getCapital());
        body.add("populationCount", String.valueOf(country.getPopulationCount()));
        body.add("currency", country.getCurrency());


        HttpEntity<MultiValueMap<String, String>> payload = new HttpEntity<>(body, headers);

        ResponseEntity<String> response = restTemplate.exchange(getInstance().getMainResrURL(), POST, payload, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(Country.getCountries(response.getBody()), hasItem(country));
    }

    @Test(dataProvider = "test4", dependsOnMethods = "verifyPostRequest")
    public void verifyDeleteRequest(Country country) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity payload = new HttpEntity(headers);

        ResponseEntity<String> response = restTemplate.exchange(getInstance().getMainResrURL() + country.getId(),
                DELETE, payload, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(Country.getCountries(response.getBody()),Matchers.not(hasItem(country)));
    }
}
