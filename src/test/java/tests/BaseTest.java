package tests;

import models.Country;
import org.testng.annotations.DataProvider;

/**
 * Created by Valentyn on 12/14/2015.
 */
public class BaseTest {

    @DataProvider(name = "test3")
    public static Object[][] test3() {
        return new Object[][] {{new Country(1, "PutTest", 999999, "UAH2")}};
    }

    @DataProvider(name = "test4")
    public static Object[][] test4() {
        return new Object[][] {{new Country(10, "NewCapital", 90, "UAH2")}};
    }
}
